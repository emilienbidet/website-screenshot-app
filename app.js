// Import all necesarry modules
var FileReader = require('filereader');
var fapi = require('file-api');
var fs = require('fs');
var screenshotmachine = require('screenshotmachine');

var reader = new FileReader();
var File = fapi.File;

var customerKey = 'e4491c';
var secretPhrase = '';
var options = {
  dimension : '1920x1080',
  device : 'desktop',
  format: 'jpg'
}


class WebSite {
  constructor(id, name, url) {
    this.id = id;
    this.name = name;
    this.url = url;
  }

  static constructAll(tab) {
    var websites = [];
    tab.forEach((website) => {
      websites.push(new WebSite(website[0], website[1], website[2]));
    });
    return websites;
  }
}

reader.onload = function(e) {

  var result = reader.result.split("\n");

  Array.prototype.splitElements = function() {
    for (i = 0; i < this.length-1; i++) {
      website = this[i].split(" ");
      res = [website[0]];
      name = "";
      for (j = 1; j < website.length - 1; j++) {
        name = name.concat(" ", website[j]);
      }
      res.push(name, website[website.length-1]);
      this[i] = res;
    }
    this.pop();
  }

  result.splitElements();

  var websites = WebSite.constructAll(result);
  var apiUrl;
  var output;
  websites.forEach((website) => {
    options.url = website.url;
    apiUrl = screenshotmachine.generateScreenshotApiUrl(customerKey, secretPhrase, options);
    output = website.id + "_" + website.name.toLowerCase() + "." + options.format;
    screenshotmachine.readScreenshot(apiUrl).pipe(fs.createWriteStream("output/" + output).on('close', function() {
  }));
  });

}

reader.readAsText(new File('website-list.txt'));
